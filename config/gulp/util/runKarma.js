import { Server } from 'karma';
import path from 'path';

function runKarma(failOnError = false, karmaOptions = {}) {
	return done => {
		process.env.NODE_ENV = 'test';
		new Server(Object.assign({
				configFile: path.resolve('config/karma.conf.babel.js')
			}, karmaOptions),
			err => {
				if ( err) {
					if (failOnError) {
						const error = new Error('Browser test failed!');
						error.showStack = false;
						done(error);
						process.exit(1);
					} else {
						done();
						process.exit(err ? 1 : 0);
					}

				} else {
					done();

					process.exit(0);
				}
			})
			.start();
	}
}

export default runKarma;