import gulp from 'gulp';
import git from 'gulp-git';

// create a new .git branch
export function branch(name) {
	return  git.branch(name, err => {
		if (err) {
			throw err;
		}
	});
}

// push to .git repo
export function push() {
	return git.push('origin', 'master', err => {
		if (err) {
			throw err;
		}
	});
}

// Commit changes
export function commit(msg) {
	return gulp.src('.')
		.pipe(git.add())
		.pipe(git.commit(msg));
}