import gulp from 'gulp';
import runSequence from 'run-sequence';
import sequenceComplete from '../util/sequenceComplete';

gulp.task('test', done => {
	runSequence(
		['node:cov', 'test:browser'],
		sequenceComplete(done));
});