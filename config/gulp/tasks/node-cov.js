import gulp from 'gulp';
import del from 'del';
import istanbul from 'gulp-babel-istanbul';
import coveralls from 'gulp-coveralls';
import mocha from 'gulp-mocha';

// Set up coverage and run tests
gulp.task('node:cov', ['lint'], (done) => {
	gulp.src(['src/**/*.js'])
		.pipe(istanbul())
	    .pipe(istanbul.hookRequire())
		// needed if we want this coverage report to be sent coveralls.io or somewhere else...
		.pipe(coveralls())
		.on('finish', () => {
			return gulp.src(['config/setup/node.js', 'test/node-tests/**/*node.js'], {
					read: false
				})
				.pipe(mocha({
					reporter: 'spec',
					timeout: 15000,
					globals: Object.keys({
						"expect": true,
						"mock": true,
						"sandbox": true,
						"spy": true,
						"stub": true,
						"useFakeServer": true,
						"useFakeTimers": true,
						"useFakeXMLHttpRequest": true
					}),
					ignoreLeaks: false
				}))
				.pipe(istanbul.writeReports())
				.pipe(istanbul.enforceThresholds({thresholds: {global: 100}}))
				.on('end', done);
		});
});