import gulp from 'gulp';
import path from 'path';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import bundle from '../util/bundle';
import pkg from '../../../package.json';

// Build a production bundle
gulp.task('build:es6', () => {
process.env.NODE_ENV = 'development';
process.env.min = false;

return bundle('es6', 'src/index.js')
	.pipe(source(pkg.name + '.es6.js'))
	.pipe(buffer())
	.pipe(gulp.dest('dist'));
});