import gulp from 'gulp';
import source from 'vinyl-source-stream';
import sourcemaps from 'gulp-sourcemaps';
import replace from 'gulp-replace';
import buffer from 'vinyl-buffer';
import bundle from '../util/bundle';
import pkg from '../../../package.json';

// Compile JS down to Google Closure syntax
gulp.task('compile:closure', () => {
	process.env.NODE_ENV = 'undefined';
	process.env.min = false;

	const moduleDeclaration = 'goog.module(\'' + pkg.name + '\');';

	return bundle('cjs', 'src/index.js')
		.pipe(source(pkg.name + '.closure.js'))
		.pipe(buffer())
		.pipe(sourcemaps.init({loadMaps: true}))
		.pipe(replace(/('|")use strict\1;/, moduleDeclaration))
		.pipe(replace("process.env.NODE_ENV !== 'production'", 'goog.DEBUG'))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('dist'));
});